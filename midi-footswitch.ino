#include <Bounce.h>    // Useful lib to debounce buttons and switches

// 5 ms debounce time should be enough
#define DEBOUNCE_TIME  5

// Define footswitches pins input numbers
#define SW1_PIN  3
#define SW2_PIN  18

// Define LED pin
#define LED_PIN  11

// Define MIDI note numbers for each switch (60 = C4)
#define SW1_NOTE  60
#define SW2_NOTE  72

// Define velocity for each switch
#define SW1_VELOCITY  127
#define SW2_VELOCITY  127

// MIDI channel for outgoing messages
const int channel = 16;

Bounce switchOne = Bounce(SW1_PIN, DEBOUNCE_TIME);
Bounce switchTwo = Bounce(SW2_PIN, DEBOUNCE_TIME);

void setup() {

  // Set footswitches pins to input mode with builtin pullup resistors enabled
  pinMode(SW1_PIN, INPUT_PULLUP);
  pinMode(SW2_PIN, INPUT_PULLUP);
  
  // Set LED pin to output mode
  pinMode(LED_PIN, OUTPUT);

}

void loop() {
 
 switchOne.update();
 switchTwo.update();
 
 // Send NoteOn messages when switches are pressed.
 // Note that we're watching for a falling edge because input pins have
 // builtin pullup resistors turned on thus are ACTIVE LOW.
 if (switchOne.fallingEdge()) {
   usbMIDI.sendNoteOn(SW1_NOTE, SW1_VELOCITY, channel);
   digitalWrite(LED_PIN, HIGH);
 }
 if (switchTwo.fallingEdge()) {
   usbMIDI.sendNoteOn(SW2_NOTE, SW2_VELOCITY, channel);
   digitalWrite(LED_PIN, HIGH);
 }
 
 // Send NoteOff messages when switches are pressed.
 if (switchOne.risingEdge()) {
   usbMIDI.sendNoteOff(SW1_NOTE, SW1_VELOCITY, channel);
   digitalWrite(LED_PIN, LOW);
 }
 if (switchTwo.risingEdge()) {
   usbMIDI.sendNoteOff(SW2_NOTE, SW2_VELOCITY, channel);
   digitalWrite(LED_PIN, LOW);
 }
 
 // Discard any incoming MIDI message.
 while (usbMIDI.read()) {
 }

}
