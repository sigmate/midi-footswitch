# MIDI Footswitch

## Description

This is a very simple Arduino sketch written for a Teensy 2.0 board. It allows
to use footswitch pedals (and more generally, any kind of switch, button, etc)
to send MIDI events over USB.

## How to use

The code is well documented and can be used as is, or tweaked, or modified to
suit any needs.

## License

This source code is released under the terms of the GNU GPL 2.0 License.

## Credits

Slikk Tim: original idea